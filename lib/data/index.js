'use strict';

module.exports = {
  de_DE: require('./de_DE.json'),
  en_GB: require('./en_GB.json'),
  en_US: require('./en_US.json'),
  es_ES: require('./es_ES.json'),
  fr_FR: require('./fr_FR.json'),
  hi_IN: require('./hi_IN.json'),
  nl_NL: require('./nl_NL.json')
};
