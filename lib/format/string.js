'use strict';

const Deep = require('@scola/deep');
const lodashTemplate = require('lodash.template');
const AbstractFormat = require('./abstract');

class StringFormat extends AbstractFormat {
  constructor(data) {
    super();
    this.data = data;
  }

  format(value, values, locale) {
    value = this.resolve(value, locale);
    values = values || {};

    value = typeof value === 'object' ?
      value[values.number] || value.d || null :
      value;

    return lodashTemplate(value, {
      interpolate: /{([\s\S]+?)}/g
    })(values);
  }

  resolve(value, locale) {
    locale = locale || this.i18n.getLocale();
    const [language] = locale.split('_');

    if (Deep.has(this.data, locale + '.' + value)) {
      value = Deep.get(this.data, locale + '.' + value);
    } else if (Deep.has(this.data, language + '.' + value)) {
      value = Deep.get(this.data, language + '.' + value);
    }

    return value;
  }
}

module.exports = StringFormat;
